/*
 * Framework para desenvolvimento do trabalho prático 2 - INF01142
 *
 * Autor: Daniel Henrique Grehs  (03/06/2013)
 *
 */


#ifndef MAIN_H
#define MAIN_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "parser.h"
#include "lru.h"

#define MAX_LIST_PROC 128
#define ERROR INT16_MAX
#define OK 0

#define PROCESS_T struct process_t

PROCESS_T
{
	uint16_t pid;
	uint16_t pages_number;
};

#endif

