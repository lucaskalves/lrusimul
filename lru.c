/*
 * Framework para desenvolvimento do trabalho prático 2 - INF01142
 *
 * Autor: Daniel Henrique Grehs  (03/06/2013)
 *
 * Implementação do LRU com Segunda Chance Otimizado:
 *    - Adolfo Schneider
 *    - Lucas Alves
 */


#include "lru.h"



/*---------------------------------------------------------------------------
 * ESTRUTURAS DE DADOS UTIlIZADAS NA IMPLEMENTACAO                           
 *---------------------------------------------------------------------------*/

typedef struct page_table_entry_st page_table_entry;
typedef struct process_page_tables_index_st process_page_tables_index;
typedef struct page_in_memory_st page_in_memory;
typedef struct pages_in_memory_queue_st pages_in_memory_queue;

// entrada na tabela de paginas
struct page_table_entry_st {
    int page;
    int reference_bit;
    int dirty_bit;
    int in_memory;
    int bit_valid;
    int num_page_faults;
    int num_substitutions;
    int allocated_frame;
    page_table_entry *next;
};

// entrada na lista de tabelas de pagina
struct process_page_tables_index_st {
    int pid;
    page_table_entry *page_table;
    process_page_tables_index *next;
};

// armazena as pagina que esta na memoria
struct page_in_memory_st {
    int frame;
    page_table_entry *page_entry;
    int *reference_bit;
    int *dirty_bit;
    page_in_memory *prev;
    page_in_memory *next;
};

// fila de paginas na memoria
struct pages_in_memory_queue_st {
    page_in_memory *first;
    page_in_memory *last;
};


// inicializa a fila das paginas que estao na memoria com frames vazios
pages_in_memory_queue *initialize_in_memory_queue(int frames_number) {
    // aloca a fila de paginas na memoria
    pages_in_memory_queue *memory_queue = (pages_in_memory_queue *)malloc(sizeof(pages_in_memory_queue));
    page_in_memory *first_page = (page_in_memory *)malloc(sizeof(page_in_memory));

    first_page->frame = 0;
    first_page->page_entry = 0;
    first_page->reference_bit = 0;
    first_page->dirty_bit = 0;
    first_page->prev = 0;
    first_page->next = 0;

    page_in_memory *prev_item = first_page;
    page_in_memory *current_mem_item;

    int i;
    for(i = 1; i<frames_number; i++) {
        current_mem_item = (page_in_memory *)malloc(sizeof(page_in_memory));
        current_mem_item->frame = i;
        current_mem_item->page_entry = 0;
        current_mem_item->reference_bit = 0;
        current_mem_item->dirty_bit = 0;
        current_mem_item->prev = 0;
        current_mem_item->next = prev_item;

        prev_item->prev = current_mem_item;
        prev_item = current_mem_item;
    }

    memory_queue->first = first_page;
    memory_queue->last = prev_item;

    return memory_queue;
}

// cria a tabela de paginas para o processo
page_table_entry *create_page_table(int pages_number) {
    page_table_entry *new_page_table = (page_table_entry *)malloc(sizeof(page_table_entry));
    page_table_entry *current;

    new_page_table->page = 0;
    new_page_table->reference_bit = 0;
    new_page_table->dirty_bit = 0;
    new_page_table->in_memory = FALSE;
    new_page_table->bit_valid = FALSE;
    new_page_table->num_page_faults = 0;
    new_page_table->num_substitutions = 0;
    new_page_table->allocated_frame = -1;
    new_page_table->next = 0;

    // cria uma nova entrada para cada pagina do processo
    page_table_entry *prev = new_page_table;

    int i;
    for(i=1; i < pages_number; i++) {
        current = (page_table_entry *)malloc(sizeof(page_table_entry));
        current->page = i;
        current->reference_bit = 0;
        current->dirty_bit = 0;
        current->in_memory = FALSE;
        current->bit_valid = FALSE;
        current->num_page_faults = 0;
        current->num_substitutions = 0;
        current->allocated_frame = -1;
        current->next = 0;

        prev->next = current;
        prev = current;
    }

    return new_page_table;
}

// adiciona a tabela de paginas ao indice de tabelas
int add_page_table_to_index(int pid, process_page_tables_index **index, page_table_entry *new_page_table) {
    process_page_tables_index *new_page_table_index_entry;
    new_page_table_index_entry = (process_page_tables_index *)malloc(sizeof(process_page_tables_index));
    new_page_table_index_entry->pid = pid;
    new_page_table_index_entry->page_table = new_page_table;
    new_page_table_index_entry->next = 0;

    if(*index == 0) {
        *index = new_page_table_index_entry;

    } else {
        // procura pelo ultimo item na lista
        process_page_tables_index *last = *index;
        while(last->next != 0) {
            last = last->next;
        }

        // adiciona ao final da lista
        last->next = new_page_table_index_entry;
    }

    return 0;
}

// procura pela tabela de paginas do processo pid
page_table_entry *search_page_table(int pid, process_page_tables_index *index) {
    process_page_tables_index *current_index = index;
    while(current_index != 0 && current_index->pid != pid) {
        current_index = current_index->next;
    }

    if(current_index != 0) {
        return current_index->page_table;
    } else {
        return 0;
    }
}

// acha uma pagina para ser substituida utilizando o LRU otimizado
page_in_memory *search_lru_page(pages_in_memory_queue *in_memory_pages_queue) {
    page_in_memory *frame = in_memory_pages_queue->first;
    int encontrou = FALSE;
    while(!encontrou) {
        if(*(frame->reference_bit) == 0 && *(frame->dirty_bit) == 0) {
            encontrou = TRUE;

            // passa os frames a frente do frame para o final da fila
            if(in_memory_pages_queue->first != frame) {
                // define o primeiro frame para o final
                in_memory_pages_queue->first->next = in_memory_pages_queue->last;
                in_memory_pages_queue->last->prev = in_memory_pages_queue->first;
               
                // define o frame seguinte ao frame encontrado para o final da fila
                in_memory_pages_queue->last = frame->next;
                in_memory_pages_queue->last->prev = 0;
                frame->next = 0;

                // passa o frame encontrado para o inicio da fila
                in_memory_pages_queue->first = frame;
            }

        } else {
            if(*(frame->reference_bit) == 1) {
                *(frame->reference_bit) = 0;
            } else {
                *(frame->dirty_bit) = 0;
            }

            if(frame->prev != 0) {
                // pega o frame anterior na fila
                frame = frame->prev;
            } else {
                // volta a busca pro inicio da fila
                frame = in_memory_pages_queue->first;
            }
        }
    }

    return frame;
}

// retorna o primeiro frame livre no sistema
page_in_memory *search_free_frame(pages_in_memory_queue *in_memory_pages_queue) {
    // procura por posicao livre na fila de itens na memoria
    page_in_memory *frame = in_memory_pages_queue->first;
    while(frame->page_entry != 0) {
        frame = frame->prev;
    }

    return frame;
}

// procura pela pagina na tabela
page_table_entry *search_page(page_table_entry *page_table, int page) {
    page_table_entry *page_entry =  page_table;
    while(page_entry != 0 && page_entry->page != page) {
        page_entry = page_entry->next;
    }

    return page_entry;
}

// substitui a pagina do frame passado, passando a pagina atual para swap
void change_page_from_frame(pages_in_memory_queue *in_memory_pages, page_in_memory *frame, page_table_entry *new_page) {
    frame->page_entry->in_memory = FALSE;
    frame->page_entry->allocated_frame = -1;
    frame->page_entry->num_substitutions += 1;
    frame->page_entry->reference_bit = 0;
    frame->page_entry->dirty_bit = 0;

    frame->page_entry = new_page;
    frame->reference_bit = &(new_page->reference_bit);
    frame->dirty_bit = &(new_page->dirty_bit);

    if(in_memory_pages->last != frame) {
        // passa esse frame para o final da fila
        if(in_memory_pages->first == frame) {
            in_memory_pages->first = frame->prev;
        }

        if(frame->prev != 0) {
            frame->prev->next = frame->next;
        }

        if(frame->next != 0) {
            frame->next->prev = frame->prev;
        }

        frame->next = in_memory_pages->last;
        in_memory_pages->last->prev = frame;
        frame->prev = 0;
        in_memory_pages->last = frame;
    }
}


/*---------------------------------------------------------------------------*
 * VARIAVEIS DE CONTROLE DO SIMULADOR
 *---------------------------------------------------------------------------*/

int max_frames = 0; // numero de frames na RAM
int used_frames = 0; // numero de paginas utilizadas
pages_in_memory_queue *in_memory_pages = 0; // armazena as paginas que estao na memoria
process_page_tables_index *page_tables_index = 0; // armazena os enderecos das tabelas de paginas dos processos



/*---------------------------------------------------------------------------*
 * IMPLEMENTACAO DA LRU
 *---------------------------------------------------------------------------*/

int memsize(int frames_number)
{
	printf("\nImplementacao do MEMSIZE com frames_number = %d\n",frames_number);
    
    max_frames = frames_number;
    in_memory_pages = initialize_in_memory_queue(frames_number);

	return 0;
}

int createproc(int pid, int pages_number)
{
	printf("\nImplementacao do CREATEPROC com pid = %d e frames_number = %d\n",pid ,pages_number);

    // cria a tabela de paginas para o processo
    page_table_entry *new_page_table = create_page_table(pages_number);
    // adiciona a tabela criada ao indice de tabelas    
    add_page_table_to_index(pid, &page_tables_index, new_page_table);

	return 0;
}

int read(int page, int pid)
{
    /* deve posicionar um bit de "página acessada" (bit_valido) na tabela de página de vocês */
	printf("\nImplementacao do READ com page = %d e pid = %d\n",page, pid);

    // procura pela tabela de paginas do processo
    page_table_entry *process_page_table = search_page_table(pid, page_tables_index);

    // nao encontrou a tabela de paginas do processo
    if(process_page_table == 0) {
        return -1;
    }

    // procura a pagina na tabela de paginas do processo
    page_table_entry *page_entry = search_page(process_page_table, page);

    // nao encontrou a pagina com o numero passado
    if(page_entry == 0) {
        return -2;
    }

    if(page_entry->in_memory == FALSE) {

        page_in_memory *frame;

        if(used_frames >= max_frames) {

            // aciona o LRU para encontrar a pagina que devera ser substituida
            frame = search_lru_page(in_memory_pages);
            
            // troca a pagina do frame pela pagina atual
            change_page_from_frame(in_memory_pages, frame, page_entry);

        } else {

            // procura por um frame livre
            frame = search_free_frame(in_memory_pages);

            if(frame == 0) {
                return -3;
            }

            frame->page_entry = page_entry;
            frame->reference_bit = &(page_entry->reference_bit);
            frame->dirty_bit = &(page_entry->dirty_bit);

            // incrementa o numero de paginas alocadas
            used_frames += 1;
        }

        page_entry->num_page_faults += 1;
        page_entry->allocated_frame = frame->frame;
        page_entry->bit_valid = TRUE;
        page_entry->in_memory = TRUE;
    
    } else {
        page_entry->reference_bit = 1;
    }


	return 0;
}

int write(int page, int pid)
{
    /* deve posicionar um bit de "página acessada" (bit_valido) na tabela de página de vocês */
	printf("\nImplementacao do WRITE com page = %d e pid = %d\n",page, pid);

    // procura pela tabela de paginas do processo
    page_table_entry *process_page_table = search_page_table(pid, page_tables_index);

    // nao encontrou a tabela de paginas do processo
    if(process_page_table == 0) {
        return -1;
    }

    // procura a pagina na tabela de paginas do processo
    page_table_entry *page_entry = search_page(process_page_table, page);

    // nao encontrou a pagina com o numero passado
    if(page_entry == 0) {
        return -2;
    }

    if(page_entry->in_memory == FALSE) {

        page_in_memory *frame;

        if(used_frames >= max_frames) {

            // aciona o LRU para encontrar a pagina que devera ser substituida
            frame = search_lru_page(in_memory_pages);
            
            // troca a pagina do frame pela pagina atual
            change_page_from_frame(in_memory_pages, frame, page_entry);

        } else {

            // procura por um frame livre
            frame = search_free_frame(in_memory_pages);

            if(frame == 0) {
                return -3;
            }

            frame->page_entry = page_entry;
            frame->reference_bit = &(page_entry->reference_bit);
            frame->dirty_bit = &(page_entry->dirty_bit);

            // incrementa o numero de paginas alocadas
            used_frames += 1;
        }

        page_entry->num_page_faults += 1;
        page_entry->allocated_frame = frame->frame;
        page_entry->in_memory = TRUE;
        page_entry->bit_valid = TRUE;
    } else {
        page_entry->dirty_bit = 1;
    }


	return 0;
}

int get_page_entry(int pid, int i, int *page, int *frame, int *page_faults, int *page_subs)
{ 
    page_table_entry *process_page_table = search_page_table(pid, page_tables_index);
    page_table_entry *page_entry = search_page(process_page_table, i);

    if(page_entry == 0) {
        return FALSE;
    }

    if ( page_entry->bit_valid == TRUE) {
    	*page = page_entry->page;
    	*frame = page_entry->allocated_frame; /* deve ser o quadro alocado na simulação */
    	*page_faults = page_entry->num_page_faults; /* deve ser o número de page_faults gerado por essa página na simulação */
    	*page_subs = page_entry->num_substitutions; /* deve ser o número de substituições que esta página sofreu na simulação */
        return TRUE;
   }

   return FALSE;
}