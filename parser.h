#ifndef INTERPRETER_H
#define INTERPRETER_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#define ERROR INT16_MAX
#define OK 0
#define END 5

#define TOKEN_T struct token_t
#define LINE_SIZE 32

#define MEMSIZE 1
#define CREATEPROC 2
#define READ 3
#define WRITE 4

TOKEN_T
{
	uint16_t name;
	uint16_t parameter_1;
	uint16_t parameter_2;
};

uint16_t parser_init(uint8_t* fileName);
void parser_end(void);

TOKEN_T parser_next_token(void);
void parser_end(void);
#endif
