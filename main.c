/*
 * Framework para desenvolvimento do trabalho prático 2 - INF01142
 *
 * Autor: Daniel Henrique Grehs  (03/06/2013)
 *
 */

#include "main.h"

int main (int argc, char* argv[])
{
	FILE* logFile = NULL;
	TOKEN_T token = {0,0,0};
	PROCESS_T listProc[MAX_LIST_PROC];
	uint16_t i_list = 0;
	uint16_t i = 0;
	uint16_t j = 0;
	int page = 0;
	int frame = 0;
	int page_faults = 0;
	int page_subs = 0;
		
	//Verificacao de parametros.
	if(argc < 2)
	{
		printf("\nUso: ./simulador fileName\n");
		return ERROR;
	}
	
	//Verificacao de abertura.
	if(parser_init((uint8_t*) argv[1]) == ERROR)
	{
		printf("\nErro na abertura do arquivo %s \n", argv[1]);
		return ERROR;
	}
	
	//Leitura dos tokens.
	do
	{
		token = parser_next_token();
		
		switch(token.name)
		{
			case MEMSIZE: 		memsize((int)token.parameter_1);
						break;
			case CREATEPROC:	listProc[i_list].pid = token.parameter_1;
						listProc[i_list].pages_number = token.parameter_2;
						i_list++;
						createproc((int)token.parameter_1,(int)token.parameter_2); 
						break;
			case READ: 		read((int)token.parameter_1, (int)token.parameter_2);
						break;
			case WRITE: 		write((int)token.parameter_1, (int)token.parameter_2);
						break;
			default:		break;		
		}
		
	} while(token.name != ERROR);
	parser_end();

	//Geracao do log.
	i = 0;
	logFile = fopen("log.txt","w");
	if(logFile == NULL)
	{
		printf("\nErro na abertura do arquivo log.txt\n");
		return ERROR;
	}
	
	while(i_list != i)
	{
		fprintf(logFile,"PROCESSO %d\n", listProc[i].pid);
		fprintf(logFile,"Página\tQuadro\tNroPageFault\tNroSubst\n");

		j = 0;

		while(listProc[i].pages_number != j)
		{
		    if (get_page_entry((int)listProc[i].pid,(int)j,&page,&frame,&page_faults,&page_subs) == TRUE )
			fprintf(logFile,"%d\t   %d\t    %d\t\t    %d\n",page,frame,page_faults,page_subs);
		    j++;
		}
		fprintf(logFile,"\n");
		i++;
	}
	
	fclose(logFile);
	return OK;
}
