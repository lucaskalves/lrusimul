## Define o compilador utilizado
CC = gcc -Wall

## Chamadas das subrotinas
all:    $(info ) clean main lru parser build


## Subrotina para montar o executavel	
build:
	$(CC) -o simulador main.o lru.o parser.o


## Subrotinas para compilar cada .c		
main:
	$(info )
	$(info - Making new files ...)
	$(CC) -c main.c
	
parser:	
	$(CC) -c parser.c

lru:
	$(CC) -c lru.c

clean: 
	rm -f *.o simulador log.txt
	$(info - Removing old files ...)	
