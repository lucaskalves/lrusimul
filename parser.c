#include "parser.h"

FILE* inputFile = NULL;

void parser_end(void)
{
	fclose(inputFile);
}

uint16_t parser_init(uint8_t* fileName)
{
	inputFile = fopen ((char*) fileName,"r");

	if(inputFile == NULL)
	{
		return ERROR;
	}

	return OK;
}

TOKEN_T parser_next_token(void)
{
	TOKEN_T token = {ERROR,ERROR,ERROR};
	uint8_t string[LINE_SIZE] = {0};
	uint8_t stringPart1[LINE_SIZE] = {0};
	uint8_t stringPart2[LINE_SIZE] = {0};
	uint8_t stringPart3[LINE_SIZE]= {0};
	uint16_t i_ant = 0;
	uint16_t i_dep = 0;

	//Leitura de linha.
	fgets ((char*) string , LINE_SIZE , inputFile);
	if( memcmp(string,"\n",strlen("\n")) == 0 || feof(inputFile) != 0)
	{
		token.name = ERROR;
		token.parameter_1 = ERROR;
		token.parameter_2 = ERROR;
		return token;
	}

	//Seleciona a primeira expressao.
	while(string[i_dep] != ' ' && string[i_dep] != '\t' && string[i_dep] != '\n')
		i_dep++;

	//Copia a primeira expressao pra stringPart1.
	memcpy( stringPart1, &string[i_ant] , i_dep);

	//Percorre espacos e tabs extras.
	while( (string[i_dep] == ' ' || string[i_dep] == '\t') && string[i_dep] != '\n' )
		i_dep++;

	//Seleciona a segunda expressao.
	i_ant = i_dep;
	while(string[i_dep] != ' ' && string[i_dep] != '\t' && string[i_dep] != '\n')
		i_dep++;

	//Copia a segunda expressao pra stringPart2.
	memcpy( stringPart2, &string[i_ant] , i_dep-i_ant);

	//Percorre espacos e tabs extras.
	while( (string[i_dep] == ' ' || string[i_dep] == '\t') && string[i_dep] != '\n' )
		i_dep++;	

	//Seleciona a terceira expressao.
	i_ant = i_dep;
	while(string[i_dep] != ' ' && string[i_dep] != '\t' && string[i_dep] != '\n')
		i_dep++;

	//Copia a terceira expressao pra stringPart3.
	memcpy( stringPart3, &string[i_ant] , i_dep-i_ant);


	//Define o token da linha.
	if(memcmp(stringPart1,"MEMSIZE",strlen((char*) stringPart1)) == 0)
	{
		token.name = MEMSIZE;
		token.parameter_1 = atoi((char*) stringPart2);
		token.parameter_2 = atoi((char*) stringPart3);
	}

	if(memcmp(stringPart1,"CREATEPROC",strlen((char*) stringPart1)) == 0)
	{
		token.name = CREATEPROC;
		token.parameter_1 = atoi((char*) stringPart2);
		token.parameter_2 = atoi((char*) stringPart3);
	}

	if(memcmp(stringPart1,"READ",strlen((char*) stringPart1)) == 0)
	{
		token.name = READ;
		token.parameter_1 = atoi((char*) stringPart2);
		token.parameter_2 = atoi((char*) stringPart3);
	}

	if(memcmp(stringPart1,"WRITE",strlen((char*) stringPart1)) == 0)
	{
		token.name = WRITE;
		token.parameter_1 = atoi((char*) stringPart2);
		token.parameter_2 = atoi((char*) stringPart3);
	}
	
	//Retorna o token correspondente a essa linha.
	return token;	
}