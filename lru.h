/*
 * Framework para desenvolvimento do trabalho prático 2 - INF01142
 *
 * Autor: Daniel Henrique Grehs  (03/06/2013)
 *
 */


#ifndef __lru
#define __lru

#include <stdio.h>
#include <stdlib.h>

#define TRUE  1
#define FALSE 0

int memsize(int frames_number);
int createproc(int pid, int pages_number);
int read(int page, int pid);
int write(int page, int pid);
int get_page_entry(int pid, int i, int *page, int *frame, int *page_faults, int *page_subs);


#endif
